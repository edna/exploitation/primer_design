# PROTOCOL : Designing a PCR primer pair to target a single species

### Step 1

Identify the target species for eDNA detection. Make a list including the target species, sympatric (co-occurring) species of the same taxon and closely related allopatric species (living in other geographical areas).

### Step 2

Identify available mitochondrial sequences (COI, 12S, 16S, etc.) from the NCBI’s Nucleotide Database (https://www.ncbi.nlm.nih.gov/nucleotide/) for the species on your list (step 1).

### Step 3

Extract the selected mitochondrial sequences from NCBI. To save a sequence : select **Send to**, choose **Complete record**, **File** and download format as either **GenBank** or **FASTA**, and then **Create File**. These sequences are now saved to the computer.

### Step 4

Align the sequences of each gene region separately using alignment software, such as the Geneious Prime software (https://www.geneious.com). Create separate folders for each gene region. Use the **Multiple alignment** tool to create a nucleotide alignment of the selected sequences. 

### Step 5

Set the following parameters (Klymus *et al.*, 2020) in the Geneious Prime software :
- Melting temperature (Tm) of the primer : 60°C ≤ Tm ≤ 64°C 
- Maximum Tm difference of 2°C between the two primers
- The Tm of the probe should be 6-8°C higher than the primers
- Set the annealing temperature (Ta) of the qPCR reaction 5 °C below the melting temperature, around 55-60 °C.
- Percentage of G/C nucleotides in the primers : 35% ≤ %GC ≤ 65% 
- Have at least 2 G or C bases in the last 5 nucleotides of each primer at the 3' end, where the DNA polymerase binds and elongation begins. It will increase specificity as it would help the primer to make a stronger bond.
- Ensure that there is no G at the 5' end of the probe, as this will interfere with the fluorescent signal emitted by the probe.
- GC clamp = 2
- Avoid repeats of one or two bases more than 4 times (it can cause issues with mispriming).
- Primer length: 18 bp - 25 bp
- Probe length: 20 bp - 25 bp
- Barcode (amplicon) length : 100 bp - 250 bp
- Check that the primers and the probe do not form a hairpin structure or dimers, to ensure good hybridisation; check for self-dimerisation or dimerisation between the forward and reverse.

The Geneious Prime software will return primer pairs and TaqMan probes that meet all these criteria.

### Step 6

Assess specificity by testing primer amplification *in silico*. For each primer pair, identify the species whose DNA will potentially be amplified by these primers. To do this, use the ecoPCR program by following the instructions available at https://gitlab.mbb.univ-montp2.fr/edna/exploitation/primer_specificity_ecopcr.  
This ecoPCR program will perform an *in silico* PCR with a given primer pair and thus provide an exhaustive list of all the sequences in the EMBL database that are amplified, with their accession number and corresponding taxon. This allows to check whether the primer pair tested amplifies also DNA from other species or not. 
(NOTE : we allow up to 3 mismatches in each primer.)

### Step 7

If the primer pair amplifies the DNA of the target species but also the DNA of various other species : find out if these species are present in the home range of the target species. For this, use species databases (e.g. Fishbase).

### Step 8

Order your designed primers and probes from a company that makes oligos. (E.g. https://www.eurogentec.com/en/)

### Step 9

Test the selected primer pair on tissues of the target species to verify *in vitro* that the target DNA barcode is well amplified by these designed primers.


### REFERENCE
Klymus, K. E., DV, R. R., Thompson, N. L., & Richter, C. A. (2020). Development and Testing of Species-specific Quantitative PCR Assays for Environmental DNA Applications. *Journal of Visualized Experiments: Jove*, (165). https://doi.org/10.3791/61825 




